package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Begin Server Log.");

        System.out.println("By default, server port is 110. Do you want change it? y/n");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();

        int port = 0;
        boolean changePort = false;
        if (answer.equals("y")) {
            System.out.println("Please enter new port");
            port = scanner.nextInt();
            changePort = true;
        }

        Server server = new Server();

        if (changePort) {
            server.setPortNumber(port);
        }

        if (server.startServerSocket()) {
            server.acceptClientConnections();
        }
    }
}
