package com.company;

public class CommandInterpreter {
    private final Database db;
    private String usernameToAuthWithPassword;
    private boolean previousCommandWasASuccessfulUSERAuth;
    private InterpreterState currentState;
    private boolean quitCommand;

    public enum POP3Command {
        USER, PASS, STAT, LIST, RETR, DELE, TOP, UIDL, RSET, QUIT, NONE, CAPA
    }

    public enum InterpreterState {
        AUTHORIZATION, TRANSACTION, UPDATE
    }

    public CommandInterpreter() {

        db = new Database();
        usernameToAuthWithPassword = "";
        previousCommandWasASuccessfulUSERAuth = false;
        currentState = InterpreterState.AUTHORIZATION;
        quitCommand = false;
    }

    public boolean checkInitialStatus() {
        return db.isConnectionValid();
    }

    public boolean isQuitCommand() { return quitCommand; }

    public void closeInterpreter() {
        db.closeConnection();
    }

    private String handleAuth(POP3Command command, String[] arguments, String input) {
        String response = null;
        switch (command) {
            case USER:
                if (arguments.length != 2)
                    return "-ERR Syntax -> USER mailboxName(required)";

                String username = arguments[1];
                response = authenticateUsername(username);
                break;

            case PASS:
                if (input.length() < 6)
                    return "-ERR Syntax -> PASS password(required)";

                response = authenticatePassword(input);
                previousCommandWasASuccessfulUSERAuth = false;
                break;

            case QUIT:

                if (arguments.length != 1)
                    return "-ERR This command has no arguments";

                response = "+OK POP3 Server Connection Terminated\n.";
                db.closeConnection();
                usernameToAuthWithPassword = null;
                previousCommandWasASuccessfulUSERAuth = false;
                quitCommand = true;
                break;
            case CAPA:
                if (arguments.length != 1)
                    return "-ERR This command has no arguments";

                response = "+OK Capability list follows\nSTLS\nTOP\nUSER\nLOGIN-DELAY 60\nPIPELINING\nEXPIRE NEVER\n" +
                        "UIDL\n" +
                        "RESP-CODE\n.";
                break;

            default:
                response = "-ERR User needs to be authenticated";
                previousCommandWasASuccessfulUSERAuth = false;
                break;
        }
        return response;
    }

    public String handleInput(String input) {

        if (!db.isConnectionValid())
            return "-ERR Database Connection Terminated Unexpectedly";

        String response = null;

        String[] arguments = input.split(" ");

        if (!lengthOfArgumentsIsValid(arguments)) {
            return "-ERR One of the arguments in the command is longer than 40 characters";
        }

        int numberOfArguments = arguments.length - 1;

        POP3Command command = setPOP3CommandEnum(arguments[0]);

        if (currentState == InterpreterState.AUTHORIZATION) {
            response = handleAuth(command, arguments, input);

        } else if (currentState == InterpreterState.TRANSACTION) {
            input = input.trim();
            int messageNumber = -1;

            if (numberOfArguments > 0 && command != POP3Command.USER
                    && command != POP3Command.PASS) {

                if (stringCanBeParsedAsAnInteger(arguments[1])) {
                    messageNumber = Integer.parseInt(arguments[1]);
                } else {
                    return "-Err Invalid Argument Type";
                }
            }

            switch (command) {
                case USER:
                case PASS:
                    response = "-ERR maildrop already locked";
                    break;

                case STAT:
                    if (numberOfArguments > 0)
                        return "-ERR This command has no arguments";
                    response = getDropListing();
                    break;

                case LIST:
                    if (numberOfArguments > 1)
                        return "-ERR Syntax -> LIST messageNumber(optional)";
                    response = getScanListing(numberOfArguments, messageNumber);
                    break;

                case RETR:
                    if (numberOfArguments != 1)
                        return "-ERR Syntax -> RETR messageNumber(required)";
                    response = getMessageBody(messageNumber);
                    break;

                case DELE:
                    if (numberOfArguments != 1)
                        return "-ERR Syntax -> DELE messageNumber(required)";

                    response = markMessageForDeletion(messageNumber);
                    break;

                case RSET:
                    if (numberOfArguments != 0)
                        return "-ERR This command has no arguements";

                    db.unmarkAllMessagesMarkedForDeletion();
                    response = "+OK maildrop has "
                            + db.getNumberOfMailsInMaildrop() + " ("
                            + db.getSizeOfMaildrop() + ") octets";
                    break;

                case TOP:
                    if (numberOfArguments != 2)
                        return "-ERR Syntax -> TOP messageNumber, numberOfLines(non negative) (both required)";

                    int numberOfLines = -1;

                    if (stringCanBeParsedAsAnInteger(arguments[2])) {
                        numberOfLines = Integer.parseInt(arguments[2]);
                    } else {
                        return "-Err Invalid Argument Type\n\n";
                    }

                    if (numberOfLines >= 0) {
                        response = getXNumberOfLinesInMessageBody(messageNumber,
                                numberOfLines);
                    } else {
                        response = "-ERR Non-Negative Number must be given with this command";
                    }
                    break;

                case UIDL:
                    if (numberOfArguments > 1)
                        return "-ERR Syntax -> UIDL messageNumber(optional)";

                    response = getUniqueIDListing(input, messageNumber);
                    break;

                case QUIT:
                    if (numberOfArguments != 0)
                        return "-ERR This command has no arguments";

                    currentState = InterpreterState.UPDATE;
                    if (db.deleteAllMessagesMarkedforDeletion()) {
                        response = "+OK Messages Deleted, POP3 Server Connection Terminated\n.";
                    } else {
                        response = "-ERR Some messages marked for deletion were not removed\n.";
                    }
                    quitCommand = true;
                    break;

                default:
                    response = "-ERR Unsupported Command Given";
                    break;
            }
        } else if (currentState == InterpreterState.UPDATE) {
            response = "- ERR POP3 Server Connection Terminates";
        }

        return response;
    }

    private String authenticateUsername(String username) {
        if (db.authenticateUser(username)) {
            usernameToAuthWithPassword = username;
            previousCommandWasASuccessfulUSERAuth = true;
            return "+OK name is a valid mailbox";
        } else {
            previousCommandWasASuccessfulUSERAuth = false;
            return "-ERR never heard of mailbox name";
        }
    }

    private String authenticatePassword(String input) {
        if (previousCommandWasASuccessfulUSERAuth) {

            String password = input.substring(5);
            if (db.authenticatePassword(usernameToAuthWithPassword,
                    password)) {

                currentState = InterpreterState.TRANSACTION;
                return "+OK maildrop locked & ready";
            } else {
                return "-ERR Invalid Password";
            }
        } else {
            return "-ERR A successful USER Command needs to be run immediately before";
        }
    }

    private String getDropListing() {
        return "+OK " + db.getNumberOfMailsInMaildrop() + " "
                + db.getSizeOfMaildrop();
    }

    private String getScanListing(int numberOfArguements, int messageNumber) {

        if (numberOfArguements == 0) {
            if (db.getNumberOfMailsInMaildrop() == 0) {
                return "+OK no messages in maildrop";
            } else {
                return "+OK scan listing follows\n"
                        + db.getScanListingOfAllMessages() + "\n.";
            }
        } else {

            if (db.getAccessStatusOfMessage(messageNumber)) {
                return "+OK " + db.getScanListingOfMessage(messageNumber);
            } else {
                return "-ERR Message not found or has been marked for deletion";
            }
        }
    }

    private String getMessageBody(int messageNumber) {
        if (db.getAccessStatusOfMessage(messageNumber)) {
            return "+OK " + db.getSizeOfMessageInOctets(messageNumber)
                    + " octets\n" + db.getMessageBody(messageNumber) + "\n.";
        } else {
            return "-ERR Message not found or has been marked for deletion or invalid argument";
        }
    }

    private String getXNumberOfLinesInMessageBody(int messageNumber,
                                                  int numberOfLines) {

        if (db.getAccessStatusOfMessage(messageNumber)) {

            return "+OK \n"
                    + db.getXNumberOfLinesInMessageBody(messageNumber,
                    numberOfLines);

        } else {
            return "-ERR Message not found or has been marked for deletion";
        }
    }

    private String markMessageForDeletion(int messageNumber) {

        if (db.getAccessStatusOfMessage(messageNumber)) {

            db.markMessageForDeletion(messageNumber);
            return "+OK Message successfully marked for deletion";

        } else {
            return "-ERR Message not found or has already been marked for deletion";
        }
    }

    private String getUniqueIDListing(String trimmedInput, int messageNumber) {

        if (trimmedInput.length() == 4) {
            return "+OK Uniquie ID listing follows\n"
                    + db.getUniqueIDListingOfAllMessages() + "\n.";
        } else {

            if (db.getAccessStatusOfMessage(messageNumber)) {
                return "+OK "
                        + db.getUniqueIDListingOfMessage(messageNumber) + "\n.";
            } else {
                return "-ERR Message not found or has been marked for deletion or invalid arguement";
            }
        }
    }


    private boolean lengthOfArgumentsIsValid(String[] arguments) {
        for (String argument : arguments) {
            if (argument.length() > 40) {
                return false;
            }
        }
        return true;
    }


    private POP3Command setPOP3CommandEnum(String command) {
        try {
            command = command.toUpperCase();
            return POP3Command.valueOf(command);

        } catch (Exception IllegalArgumentException) {
            return POP3Command.NONE;
        }
    }

    private boolean stringCanBeParsedAsAnInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
