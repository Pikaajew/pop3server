package com.company;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.Properties;

public class Database {
    private final static String DB_USERNAME = "root";
    private final static String DB_PASSWORD = "";

    private final static String DB_PORT = "3306";
    private final static String DB_TYPE = "mysql";
    private final static String DB_NAME = "pop3";
    private final static String DB_SERVER = "localhost";

    private final static String DB_ENCODING = "useUnicode=true&characterEncoding=UTF8";
    private final static String DB_TIMEZONE = "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow";

    private Connection conn;
    private boolean connected;

    private int sessionUserID;

    public Database() {
        connected = false;
        sessionUserID = -1;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            connected = true;
        } catch (SQLException e) {
            System.out.println("Database connection could not be started");
            printSQLException(e);
        } catch (ClassNotFoundException e) {
            System.err.println("Error loading driver: " + e);
            e.printStackTrace(System.err);
        }
    }

    private Connection getConnection() throws SQLException,
            ClassNotFoundException {

        Properties connectionProperties = new Properties();
        connectionProperties.put("user", DB_USERNAME);
        connectionProperties.put("password", DB_PASSWORD);

        Class.forName("com.mysql.jdbc.Driver");

        System.out.println("Connecting to a selected database");
        Connection conn = DriverManager.getConnection(
                "jdbc:" + DB_TYPE + "://" + DB_SERVER + ":" + DB_PORT + "/"
                        + DB_NAME + "?" + DB_ENCODING + DB_TIMEZONE, connectionProperties);
        System.out.println("Connected to database successfully");

        return conn;
    }

    public boolean isDatabaseConnected() {
        return connected;
    }

    public boolean isConnectionValid() {
        try {
            return conn.isValid(0);
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
    }

    public boolean authenticateUser(String username) {

        String query = "SELECT username FROM accounts WHERE logged=0;";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                if (username.equals(rs.getString("username"))) {
                    return true;
                }
            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return false;
    }

    public boolean authenticatePassword(String username, String password) {

        String query = "SELECT id, username, password FROM accounts WHERE logged=0;";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                if (username.equals(rs.getString("username"))) {

                    if (password.equals(rs.getString("password"))) {

                        sessionUserID = rs.getInt("id");

                        String setMaildropToLocked = "UPDATE accounts SET logged=1 WHERE id="
                                + sessionUserID + ";";

                        statement.executeUpdate(setMaildropToLocked);
                        conn.commit();
                        return true;

                    } else {
                        return false;
                    }
                }
            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return false;
    }

    public int getNumberOfMailsInMaildrop() {

        int numberOfMails = 0;
        String query = "SELECT mail_id FROM mail WHERE account_id="
                + sessionUserID;

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);
            numberOfMails = getRowCount(rs);

        } catch (SQLException e) {
            printSQLException(e);
        }

        return numberOfMails;
    }

    public boolean getAccessStatusOfMessage(int messageNumber) {

        int numberOfMessages = getNumberOfMailsInMaildrop();

        if (messageNumber > numberOfMessages || messageNumber == 0)
            return false;
        else
            return true;

    }

    public int getSizeOfMessageInOctets(int messageNumber) {

        int sizeOfMessageInOctets = 0;
        String query = "SELECT mail_text FROM mail WHERE account_id="
                + sessionUserID + ";";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);
            rs.absolute(messageNumber);
            sizeOfMessageInOctets = getNumberOfOctetsInString(rs
                    .getString("mail_text"));

        } catch (SQLException e) {
            printSQLException(e);
        }

        return sizeOfMessageInOctets;
    }

    public int getSizeOfMaildrop() {

        int totalSizeOfMaildrop = 0;
        String query = "SELECT mail_text FROM mail WHERE account_id="
                + sessionUserID;

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {

                totalSizeOfMaildrop += getNumberOfOctetsInString(rs
                        .getString("mail_text"));
            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return totalSizeOfMaildrop;
    }


    public String getDropListingOfMaildrop() {
        return getNumberOfMailsInMaildrop() + " " + getSizeOfMaildrop();
    }


    public String getScanListingOfMessage(int messageNumber) {
        return messageNumber + " " + getSizeOfMessageInOctets(messageNumber);
    }


    public String getScanListingOfAllMessages() {

        StringBuilder scanListing = new StringBuilder();
        String query = "SELECT mail_text FROM mail WHERE account_id="
                + sessionUserID;

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            int numberOfMails = getNumberOfMailsInMaildrop();

            while (rs.next()) {

                int messageNumber = rs.getRow();
                int sizeInOctets = getNumberOfOctetsInString(rs
                        .getString("mail_text"));

                if (messageNumber == numberOfMails) {
                    // For Nice Formatting
                    scanListing.append(messageNumber + " "
                            + sizeInOctets);
                } else {
                    scanListing.append(messageNumber + " "
                            + sizeInOctets + "\n");
                }

            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return scanListing.toString();
    }


    public String getUniqueIDListingOfMessage(int messageNumber) {

        String uidl = "";
        String query = "SELECT mail_uidl FROM mail WHERE account_id="
                + sessionUserID + ";";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);
            rs.absolute(messageNumber);
            uidl = rs.getString("mail_uidl");

        } catch (SQLException e) {
            printSQLException(e);
        }

        return messageNumber + " " + uidl;
    }


    public String getUniqueIDListingOfAllMessages() {

        StringBuilder uidlListing = new StringBuilder();
        String query = "SELECT mail_id, mail_uidl FROM mail WHERE account_id="
                + sessionUserID;

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            int numberOfMails = getNumberOfMailsInMaildrop();


            while (rs.next()) {

                int messageNumber = rs.getRow();
                String uidl = rs.getString("mail_uidl");

                if (messageNumber == numberOfMails) {
                    uidlListing.append(messageNumber + " " + uidl);
                } else {
                    uidlListing.append(messageNumber + " " + uidl + "\n");
                }

            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return uidlListing.toString();
    }


    public int getNumberOfLinesInMessageBody(int messageNumber) {

        int numberOfLines = 0;
        String query = "SELECT mail_text FROM mail WHERE account_id="
                + sessionUserID + ";";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);
            rs.absolute(messageNumber);
            String messageBody = rs.getString("mail_text");
            numberOfLines = messageBody.split("\\n").length;

        } catch (SQLException e) {
            printSQLException(e);
        }

        return numberOfLines;
    }


    public String getMessageBody(int messageNumber) {

        String messageBody = "";
        String query = "SELECT mail_text FROM mail WHERE account_id="
                + sessionUserID + ";";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);
            rs.absolute(messageNumber);
            messageBody = rs.getString("mail_text");

        } catch (SQLException e) {
            printSQLException(e);
        }

        return messageBody;
    }


    public String getXNumberOfLinesInMessageBody(int messageNumber,
                                                 int numberOfLines) {

        StringBuilder message = new StringBuilder();

        if (numberOfLines > getNumberOfLinesInMessageBody(messageNumber)) {
            return getMessageBody(messageNumber);

        } else {

            String[] messageBodyArray = getMessageBody(messageNumber).split(
                    "\\n");

            for (int i = 0; i < numberOfLines; i++) {
                if (i == numberOfLines - 1) {
                    message.append(messageBodyArray[i]);
                } else {
                    message.append(messageBodyArray[i] + "\n");
                }
            }
        }
        return message.toString();
    }


    public void markMessageForDeletion(int messageNumber) {

        String mailIDQuery = "SELECT mail_id FROM mail WHERE account_id="
                + sessionUserID + ";";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(mailIDQuery);
            rs.absolute(messageNumber);

            String deleteQuery = "DELETE FROM mail WHERE account_id=" + sessionUserID
                    + " AND " + "mail_id=" + rs.getInt("mail_id") + ";";
            statement.executeUpdate(deleteQuery);

        } catch (SQLException e) {
            rollbackCommit();
            printSQLException(e);
        }
    }


    public void unmarkAllMessagesMarkedForDeletion() {
        rollbackCommit();
    }


    public boolean deleteAllMessagesMarkedforDeletion() {

        try {
            unlockMaildrop();
            conn.commit();

        } catch (SQLException e) {
            printSQLException(e);
            rollbackCommit();
            return false;
        }

        return true;
    }

    private void unlockMaildrop() {

        try (Statement statement = conn.createStatement()) {

            String unlockMaildrop = "UPDATE accounts "
                    + "SET logged=0 WHERE id=" + sessionUserID
                    + ";";

            statement.executeUpdate(unlockMaildrop);
            conn.commit();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    public String getAllUsers() {
        String listOfCLiens = "";
        String query = "SELECT username, id FROM accounts;";

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                listOfCLiens += rs.getString("id") + " " + rs.getString("username") + "\n";
            }

        } catch (SQLException e) {
            printSQLException(e);
        }

        return listOfCLiens;
    }

    public boolean addNewUser(String user, String pass) {
        String query = "INSERT INTO accounts(username, password, logged) VALUES ('" + user + "','" + pass + "', 0)";
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate(query);
            conn.commit();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteUser(int userId) {
        Integer userIdInteger = userId;
        String query = "DELETE FROM accounts WHERE id = " + userIdInteger + ";";
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate(query);
            conn.commit();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateUsername(int userId, String username) {
        Integer userIdInteger = userId;
        String query = "UPDATE accounts SET username = '" + username + "' WHERE id = " + userIdInteger.toString() + ";";
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate(query);
            conn.commit();
        } catch (SQLException e) {
            printSQLException(e);
            return false;
        }
        return true;
    }

    public boolean updatePassword(int userId, String password) {
        Integer userIdInteger = userId;
        String query = "UPDATE accounts SET password = '" + password + "'WHERE id = " + userIdInteger.toString() + ";";
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate(query);
            conn.commit();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public void closeConnection() {
        try {
            if (conn != null) {
                unlockMaildrop();
                conn.close();
            }

        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    private int getRowCount(ResultSet rs) {

        int count = 0;

        try {
            rs.absolute(-1);
            count = rs.getRow();
            rs.beforeFirst();
        } catch (SQLException e) {
            printSQLException(e);
        }

        return count;
    }

    private int getNumberOfOctetsInString(String string) {

        int num = 0;

        try {
            final byte[] utf8Bytes = string.getBytes("UTF-8");
            num = utf8Bytes.length;
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        }

        return num;
    }

    private void rollbackCommit() {
        try {
            conn.rollback();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    private void printSQLException(SQLException ex) {

        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                if (ignoreSQLException(((SQLException) e).getSQLState()) == false) {

                    e.printStackTrace(System.err);
                    System.err.println("SQLState: "
                            + ((SQLException) e).getSQLState());

                    System.err.println("Error Code: "
                            + ((SQLException) e).getErrorCode());

                    System.err.println("Message: " + e.getMessage());

                    Throwable t = ex.getCause();
                    while (t != null) {
                        System.out.println("Cause: " + t);
                        t = t.getCause();
                    }
                }
            }
        }
    }

    private boolean ignoreSQLException(String sqlState) {

        if (sqlState == null) {
            System.out.println("The SQL state is not defined!");
            return false;
        }

        if (sqlState.equalsIgnoreCase("X0Y32"))
            return true;

        if (sqlState.equalsIgnoreCase("42Y55"))
            return true;

        return false;
    }
}
