package com.company;

import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class ClientThread implements Runnable {
    private final Socket socket;

    private final static String helloMessage = "+OK POP3 Server Ready";

    private final static String terminationMessage = "+OK POP3 Server Connection Terminated";
    private final static String terminationMessage2 = "-ERR Some messages marked for deletion were not removed";
    private final static String terminationMessage3 = "+OK Messages Deleted, POP3 Server Connection Terminated";

    private final static String databaseErrorMessage = "-ERR Database Connection Could Not Be Started";
    private final static String databaseErrorMessage2 = "-ERR Database Connection Terminated Unexpectedly";

    public ClientThread(Socket socket, int timeoutValue) {
        this.socket = socket;

        try {
            this.socket.setSoTimeout(timeoutValue * 1000);
        } catch (SocketException e) {
            System.err.println("Socket exception");
            e.printStackTrace();
            closeSocket();
        }

        System.out.println("New Client Connection Started");
    }
    @Override
    public void run() {
        try (PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8),true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

            String inputLine = null;
            String outputLine = null;

            CommandInterpreter commandInterpreter = new CommandInterpreter();

            if (commandInterpreter.checkInitialStatus()) {
                out.println(helloMessage);
            } else {
                out.println(databaseErrorMessage);
                closeSocket();
            }

            do {
                inputLine = in.readLine();
                if (inputLine == null) {
                    break;
                }
                System.out.println("Input Line: " + inputLine);
                outputLine = commandInterpreter.handleInput(inputLine);
                System.out.println("Output Line: " + outputLine);
                out.println(outputLine);

            } while (!outputLine.equals(terminationMessage)
                    && !outputLine.equals(terminationMessage2)
                    && !outputLine.equals(terminationMessage3)
                    && !outputLine.equals(databaseErrorMessage2)
                    && !commandInterpreter.isQuitCommand());

            commandInterpreter.closeInterpreter();
            closeSocket();
        } catch (IOException e) {
            System.out.println("Connection to client was stopped by server or due to inactivity");
        }
    }

    public void closeSocket() {
        try {
            System.out.println("Connection has been successfully terminated");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
