package com.company;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Scanner;

public class ServerCommandInterpreter {
    private final Database db;

    public ServerCommandInterpreter() {
        db = new Database();
    }

    public String parseServerCommand(String command, ArrayList<Pair<Thread, ClientThread>> threadArrayList) {
        switch (command) {
            case "close":
                try {
                    for (Pair<Thread, ClientThread> threadPair : threadArrayList) {
                        threadPair.getValue().closeSocket();
                        threadPair.getKey().join();
                    }
                } catch (InterruptedException e) {
                    System.out.println("ERROR - Interrupted exception");
                }
                return "close";

            case "list":
                return getListOfUsers();

            case "add":
                System.out.println("Enter login and password with space");
                Scanner scannerAdd = new Scanner(System.in);
                String loginPass = scannerAdd.nextLine();
                String[] arguments = loginPass.split(" ");
                if (arguments.length != 2) {
                    return "ERROR - expected 2 arguments";
                }
                if (!addNewUser(arguments[0], arguments[1])) {
                    return "Not found user id";
                }
                return "Success adding";

            case "delete":
                System.out.println("Enter user id to delete");
                Scanner scannerDelete = new Scanner(System.in);
                int userIdDel = scannerDelete.nextInt();
                if (!deleteUser(userIdDel)) {
                    return "Not found user id";
                }
                return "Success deletion";

            case "update":
                System.out.println("Enter user id to update");
                Scanner scannerUpdate = new Scanner(System.in);
                int userIdUpd = Integer.parseInt(scannerUpdate.nextLine());

                System.out.println("Do you want to change username? y/n");
                String answer = scannerUpdate.nextLine();
                if (answer.equals("y")) {
                    System.out.println("Enter username");
                    String login = scannerUpdate.nextLine();
                    if (updateUsername(userIdUpd, login)) {
                        return "Success login update";
                    } else {
                        return "Not found user id to update login";
                    }
                }

                System.out.println("Do you want to change password? y/n");
                answer = scannerUpdate.nextLine();
                if (answer.equals("y")) {
                    System.out.println("Enter password");
                    String login = scannerUpdate.nextLine();
                    if (updatePassword(userIdUpd, login)) {
                        return "Success password update";
                    } else {
                        return "Not found user id";
                    }
                }

                return "No more data to update";
        }
        return "Unknown Server command";
    }

    private String getListOfUsers() {
        return db.getAllUsers();
    }

    private boolean addNewUser(String username, String password) {
        return db.addNewUser(username, password);
    }

    private boolean deleteUser(int userId) {
        return db.deleteUser(userId);
    }

    private boolean updateUsername(int userId, String login) {
        return db.updateUsername(userId, login);
    }

    private boolean updatePassword(int userId, String password) {
        return db.updatePassword(userId, password);
    }
}
