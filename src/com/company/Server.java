package com.company;

import javax.net.ServerSocketFactory;
import javafx.util.Pair;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Scanner;

public class Server implements Runnable{

    private int portNumber = 110;
    private int timeoutValue = 600;
    private boolean isListening = false;
    private ServerSocket serverSocket;
    private ArrayList<Pair<Thread, ClientThread>> threadArrayList;

    public Server() {
        new Thread(this).start();
        threadArrayList = new ArrayList<>();
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public boolean startServerSocket() {
        try {
            ServerSocketFactory factory = ServerSocketFactory.getDefault();
            serverSocket = factory.createServerSocket(portNumber);
            System.out.println("Server was successfully started");
            isListening = true;
            return true;

        } catch (IOException e) {
            System.err.println("ERROR - Could not listen on port " + portNumber);
            return false;
        }
    }

    public void acceptClientConnections() {
        while (isListening) {
            try {
                if (!serverSocket.isClosed()) {
                    ClientThread clientThread = new ClientThread(serverSocket.accept(), timeoutValue);
                    Thread thread = new Thread(clientThread);
                    thread.start();
                    threadArrayList.add(new Pair(thread, clientThread));
                }
            } catch (SocketException e) {
                System.out.println("Server has been closed");
                isListening = false;
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("ERROR - Connection could not be accepted");
                stopServerSocket();
                isListening = false;
            }
        }
    }

    private void stopServerSocket() {
        if (serverSocket != null && !serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                System.err.println("ERROR - Server Socket Could Not Be Closed");
            }
        }
    }

    @Override
    public void run()  {
        ServerCommandInterpreter serverCommandInterpreter = new ServerCommandInterpreter();
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            String answer = serverCommandInterpreter.parseServerCommand(command, threadArrayList);
            if (answer.equals("close")) {
                stopServerSocket();
                return;
            } else {
                System.out.println(answer);
            }
        }
    }
}
